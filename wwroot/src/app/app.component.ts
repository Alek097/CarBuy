﻿import { Component } from '@angular/core'

import template from './app.component.html';
import './app.component.scss';

@Component(
    {
        selector: 'app-root',
        template: template,
    })
export class AppComponent {
    public name: string = 'Aleksey'
}
﻿import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { NavbarModule } from './navbar/navbar.module';

import { NgModule } from '@angular/core';

import { AppComponent } from './app.component'
@NgModule(
    {
        imports: [BrowserModule, FormsModule, NavbarModule],
        exports: [NavbarModule],
        declarations: [AppComponent],
        bootstrap: [AppComponent],
    })
export class AppModule { }
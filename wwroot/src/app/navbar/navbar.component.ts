﻿import { Component } from "@angular/core";

import template from './navbar.component.html'
import './navbar.component.scss';

@Component({
    selector: 'navigation-bar',
    template: template
})
export class NavbarComponent
{

}
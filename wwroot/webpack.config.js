﻿"use strict";

var path = require('path');

var webpack = require('webpack');
var TransferWebpackPlugin = require('transfer-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin')

var wwwrootPath = __dirname + '/../CarBuy/wwwroot/';

var common = {
    entry: {
        app: [
            './node_modules/jquery/dist/jquery.min.js',

            './node_modules/bootstrap/scss/bootstrap.scss',
            './node_modules/font-awesome/scss/font-awesome.scss',
            './Style.scss',

            './node_modules/core-js/client/shim.min.js',
            './node_modules/zone.js/dist/zone.js',
            './node_modules/reflect-metadata/Reflect.js',
            './node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
            './src/main.ts',
        ],
    },
    output: {
        path: wwwrootPath,
        filename: './[name].js'
    },
    resolve: {
        extensions: ['.ts', 'tsx', '.js']
    },
    module: {
        rules: [
            {
                test: require.resolve(path.resolve(__dirname, './node_modules/jquery/dist/jquery.min.js')),
                use: [
                    {
                        loader: 'expose-loader',
                        options: '$'
                    },
                    {
                        loader: 'expose-loader',
                        options: 'jQuery'
                    }
                ]
            },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
                options: {
                    transpileOnly: true
                }
            },
            {
                test: /\.(scss|css)$/,
                use: [{
                    loader: "style-loader"
                },
                {
                    loader: "css-loader"
                },
                {
                    loader: "sass-loader"
                }]
            },
            {
                test: /\.html$/,
                exclude: /node_modules/,
                loader: "html-loader?exportAsEs6Default"
            },
            {
                test: /\.(png|jpg|gif|ttf|woff|svg|woff2|eot|otf)$/,
                loader: 'url-loader',
                options: {
                    limit: 1024 * 1000
                }
            },
        ]
    },

    plugins: [
        new TransferWebpackPlugin([
        ]),
        new CopyWebpackPlugin([
            { from: 'index.html', to: wwwrootPath + 'index.html' }
        ]),
    ],
};

var production = {
    plugins: [
        new UglifyJsPlugin()
    ]
}

var development = {
    devtool: "source-map",
}

module.exports = env => {
    if (env.production) {
        var productionObject = Object.assign({}, common);

        productionObject.plugins = productionObject.plugins.concat(production.plugins);

        return productionObject;
    }
    else if (env.development) {
        return Object.assign({}, common, development);
    }
}